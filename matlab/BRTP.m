function [Image_G,Image_Y,Image_Q,Image_P,count] = ...
         BRTP(M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny)     
% goal:  backward ray-tracing FUNCTION for a pencil beam of rays with
%        rectangular cross sections
% goal:  plus forward integration from the polarization after BRT
% note:  did not consider higher ordeer images for this case.
% note:  inc_disk should be in radian

%------------------------------------------------------------------
    r_ISCO  = r_MS_plus(M,a);       % r_ISCO=6/4.23/1.24M if a=0/0.5/0.998M        
    dx_grid = x_size/nx;
    dy_grid = y_size/ny;
    
    Image_P = zeros(2*ny+1,2*nx+1,3);       % store delta, and chi (radian)
    Image_Q = zeros(2*ny+1,2*nx+1);         % relative error in Carter
    Image_G = zeros(2*ny+1,2*nx+1);         % E_observer/E_emitter
    Image_Y = zeros(2*ny+1,2*nx+1,8);       % (x^a,p_a)     
% configure the ode solver 'ode45', a 4/5 order Runge-Kutta algorithm
                                       
    err_rel = 1E-9;                 % relative error for Runge-Kutta
    err_abs = [1E-7,1E-7,1E-9,1E-9,1E-9,1E-7,1E-7,1E-9];       % absolute error
    max_dxi = 0.1*r_obs;            % maximum stepsize of xi (affine para)
    options = odeset('RelTol',   err_rel, ...
                     'AbsTol',   err_abs, ...
                     'Events',   @disk_events, ...
                     'MaxStep',  max_dxi);         
                 
    para    = [M,a];
    xi_max  = 2*r_obs;              % goes to the other side of the disk
    XI_span = [0, -xi_max];         % negative for backward raytracing
  
% -----  backward ray-tracing starts here ---------  

    numb_disk  = 0;                 % count nubmer of rays hitting the disk
    numb_hole  = 0;                 % count rays entering the horizon
    numb_high  = 0;                 % count rays for higher order images
    
    %--------------- all rays start from same spacial location...
    t0     = 0;                     % backward in time.
    r0     = r_obs;
    theta0 = inc_disk;              % in radian already        
    phi0   = 0;                     % azimuthal symmetry   
    X0     = [t0,r0,theta0,phi0];
    lx     = 2*nx+1;
    ly     = 2*ny+1;
%    parfor i_x = 1:2*nx+1          % this one is OK, but, not the next                  
%       for i_y = 1:2*ny+1          % range must be plain expression        
    parfor i_x = 1 : lx             % Aug 12 2014   
       for i_y = 1 : ly         % Aug 12 2014         
          p_r0     = r_obs;         % positive, for backward ray-tracing
          p_phi0   = dx_grid*(nx-i_x+1);   % phi increases when ix increases  
          p_theta0 = dy_grid*(ny-i_y+1);   % y axis pointing downward here !!! 
          
          p3 = [p_r0,p_theta0,p_phi0];      % 3D un-normalized vector
          p4 = p3_to_p4(p3,r0,theta0,M,a);  % p4 is covariant 4-vector.
          
          Y0 = zeros(1,8);
          Y0(1,1:4) = X0;
          Y0(1,5:8) = p4;           % initial 8D coordinates for ode45
          
          Q_carter0 = Carter(Y0,a); % initial Carter constant
                        
          [XI,Y,XIe,Ye,Ie] = ode45(@Y_prime,XI_span,Y0,options,para);
            
          if Ie == 2
             numb_hole = numb_hole + 1;   
          elseif Ie == 1
             if (Ye(1,2) <= r_disk) && (Ye(1,2) >= r_ISCO)
               numb_disk = numb_disk + 1;
               Q_carter = Carter(Ye(1,:),a);
               err_Q    = abs((Q_carter - Q_carter0)/Q_carter0);
               E_obs    = energy_ZAMO(r0,theta0,M,a,p4); %observed energy 
               E_disk   = energy_CO_plus(Ye(1,:),M,a);
               redshift = E_obs/E_disk;
               Image_Q(i_y,i_x) = err_Q;    % store accuracy check
               Image_G(i_y,i_x) = redshift;
               Image_Y(i_y,i_x,:) = Ye(1,:); % this finishes the backward part.

               [weight, delta, E_source] = polarization(Ye(1,:),M,a);
               E4_obs = FPP4(XIe(1),Ye(1,:),E_source,M,a,r_obs);
               %E_obs = FPP(XI,Y,E_source,M,a);               
               chi   = polar_obs(X0,E4_obs.',M,a); 
               Image_P(i_y,i_x,:) = [weight,delta,chi];               
             end
          end   
       end
    end
    
    count = [numb_disk,numb_hole,numb_high];
        
