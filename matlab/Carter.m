function Q_carter = Carter(Y,a)
% compute Carter's constant for massless particles
% Y[1:8] instead of Y[0:7], so [t,r,theta,phi, p_t, p_r, p_theta,p_phi]
% note that Y[5:8] is the covariant 4-momentum vector, say p_a, not p^a.

% important: this little function is vectorized. 
%            input:    Y as the output of ode solver for one geodesic
%            output:   array Q_carter for accuracy of the Runge-Kutta 

    theta   = Y(:,3);
    p_t     = Y(:,5);
    p_theta = Y(:,7);
    p_phi   = Y(:,8);
    
    cos2    = cos(theta).^2;   % sin, cos was vectorized intrinsically
    sin2    = sin(theta).^2;
    
    Q_carter = p_theta.^2 + cos2.*(p_phi.^2./sin2 - (a*p_t).^2);

end

