function E_obs = FPP(XI,Y,E_source,M,a)
%   compute the Gravitionlly Faraday rotated polarization vector.
% 
    dim     = size(Y);
    n_point = dim(1);
    E_curr  = E_source;
    E_next  = E_curr; 
    for j = 0:n_point-2
      d_xi  = XI(n_point-j-1) - XI(n_point-j);
	  r     =  Y(n_point-j, 2);
      theta =  Y(n_point-j, 3);
      p_low =  Y(n_point-j, 5:8);
      g_up  = metric_up(r,theta,M,a);
      p_up  = g_up*p_low.';
      Gamma = connexion(M,a,r,theta);
      for k = 1:4
          dE = 0;
          for p = 1:4
              for q = 1:4
                  dE = dE + Gamma(k,p,q)*p_up(p,1)*E_curr(q,1);
              end
          end
          dE = -dE*d_xi;
          E_next(k,1) = E_curr(k,1) + dE;
      end	  
      E_curr = E_next;
    end
    E_obs = E_curr;
end

