function E4_obs = FPP4(XI_disk,Y_disk,E_source,M,a,r_obs)
% 12D forward propagator for the polarization vector.
% assume Y_disk a row vector, E_source a column vector

    Y0 = [Y_disk, E_source.'];
    
    % configure the ode solver 'ode45', a 4/5 order Runge-Kutta algorithm                                       
    err_rel = 1E-8;                 % relative error for Runge-Kutta
    err_abs = [1E-7,1E-7,1E-9,1E-9,1E-9,1E-7,1E-7,1E-9,1E-7,1E-7,1E-7,1E-7];       % absolute error
    max_dxi = 0.1*r_obs;            % maximum stepsize of xi (affine para)
    options = odeset('RelTol',   err_rel, ...
                     'AbsTol',   err_abs, ...
                     'Events',   @obs_events, ...
                     'MaxStep',  max_dxi);         
                 
    para    = [M,a,r_obs];
    xi_max  = 1.5*r_obs;           % BRT toward the observer
    XI_span = [0, xi_max];         % positive for  raytracing
    [XI,Y,XIe,Ye,Ie] = ode45(@Y_prime_12D,XI_span,Y0,options,para);
    if Ie == 1
        E4_obs  = Ye(1,9:12);
    else
        E4_obs  = zeros(1,4);
    end
end

