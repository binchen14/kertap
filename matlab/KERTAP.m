function varargout = KERTAP(varargin)
% KERTAP MATLAB code for KERTAP.fig
%      KERTAP, by itself, creates a new KERTAP or raises the existing
%      singleton*.
%
%      H = KERTAP returns the handle to a new KERTAP or the handle to
%      the existing singleton*.
%
%      KERTAP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KERTAP.M with the given input arguments.
%
%      KERTAP('Property','Value',...) creates a new KERTAP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before KERTAP_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to KERTAP_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help KERTAP

% Last Modified by GUIDE v2.5 17-Sep-2012 10:07:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @KERTAP_OpeningFcn, ...
                   'gui_OutputFcn',  @KERTAP_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before KERTAP is made visible.
function KERTAP_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to KERTAP (see VARARGIN)

handles.a           = 0.998;
handles.inc_disk    = 75;
handles.r_obs       = 1E6;
handles.r_disk      = 20;
handles.x_size      = 25;
handles.y_size      = 25;
handles.nx          = 25;
handles.ny          = 25;
handles.num_workers = 2;
handles.Gamma       = 2.0;
handles.n           = 3.0;

% Choose default command line output for KERTAP
handles.output   = hObject;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes KERTAP wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = KERTAP_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
handles.a = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
handles.inc_disk = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
handles.r_obs = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
handles.r_disk = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
handles.x_size = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
handles.y_size = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double
handles.nx = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
handles.ny = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
handles.num_workers = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit26_Callback(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit26 as text
%        str2double(get(hObject,'String')) returns contents of edit26 as a double
handles.Gamma = str2double(get(hObject,'String'));
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function edit26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit27_Callback(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit27 as text
%        str2double(get(hObject,'String')) returns contents of edit27 as a double
handles.n = str2double(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit27_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function run_status_Callback(hObject, eventdata, handles)
% hObject    handle to run_status (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of run_status as text
%        str2double(get(hObject,'String')) returns contents of run_status as a double


% --- Executes during object creation, after setting all properties.
function run_status_CreateFcn(hObject, eventdata, handles)
% hObject    handle to run_status (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Q_max_Callback(hObject, eventdata, handles)
% hObject    handle to Q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Q_max as text
%        str2double(get(hObject,'String')) returns contents of Q_max as a double


% --- Executes during object creation, after setting all properties.
function Q_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function g_max_Callback(hObject, eventdata, handles)
% hObject    handle to g_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of g_max as text
%        str2double(get(hObject,'String')) returns contents of g_max as a double


% --- Executes during object creation, after setting all properties.
function g_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to g_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function g_min_Callback(hObject, eventdata, handles)
% hObject    handle to g_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of g_min as text
%        str2double(get(hObject,'String')) returns contents of g_min as a double


% --- Executes during object creation, after setting all properties.
function g_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to g_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function BRT_rate_Callback(hObject, eventdata, handles)
% hObject    handle to BRT_rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of BRT_rate as text
%        str2double(get(hObject,'String')) returns contents of BRT_rate as a double


% --- Executes during object creation, after setting all properties.
function BRT_rate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BRT_rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function wall_clock_Callback(hObject, eventdata, handles)
% hObject    handle to wall_clock (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of wall_clock as text
%        str2double(get(hObject,'String')) returns contents of wall_clock as a double


% --- Executes during object creation, after setting all properties.
function wall_clock_CreateFcn(hObject, eventdata, handles)
% hObject    handle to wall_clock (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function flux_mag_Callback(hObject, eventdata, handles)
% hObject    handle to flux_mag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of flux_mag as text
%        str2double(get(hObject,'String')) returns contents of flux_mag as a double


% --- Executes during object creation, after setting all properties.
function flux_mag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to flux_mag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function area_mag_Callback(hObject, eventdata, handles)
% hObject    handle to area_mag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of area_mag as text
%        str2double(get(hObject,'String')) returns contents of area_mag as a double


% --- Executes during object creation, after setting all properties.
function area_mag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to area_mag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function deg_polar_Callback(hObject, eventdata, handles)
% hObject    handle to deg_polar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of deg_polar as text
%        str2double(get(hObject,'String')) returns contents of deg_polar as a double


% --- Executes during object creation, after setting all properties.
function deg_polar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to deg_polar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function angle_polar_Callback(hObject, eventdata, handles)
% hObject    handle to angle_polar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of angle_polar as text
%        str2double(get(hObject,'String')) returns contents of angle_polar as a double


% --- Executes during object creation, after setting all properties.
function angle_polar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angle_polar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.a           = 0.998;
handles.inc_disk    = 75;
handles.r_obs       = 1E6;
handles.r_disk      = 20;
handles.x_size      = 25;
handles.y_size      = 25;
handles.nx          = 25;
handles.ny          = 25;
handles.num_workers = 2;
handles.Gamma       = 2.0;
handles.n           = 3.0;
set(handles.run_status,'String','waiting for input');
set(handles.edit1,'String','0.998');
set(handles.edit2,'String','75');
set(handles.edit3,'String','1E6');
set(handles.edit4,'String','20');
set(handles.edit5,'String','25');
set(handles.edit6,'String','25');
set(handles.edit7,'String','25');
set(handles.edit8,'String','25');
set(handles.edit9,'String','2');
set(handles.edit26,'String','2');
set(handles.edit27,'String','3');
set(handles.g_min,'String','');
set(handles.g_max,'String','');
set(handles.Q_max,'String','');
set(handles.wall_clock,   'String','');
set(handles.BRT_rate,     'String','');
set(handles.flux_mag,     'String','');
set(handles.area_mag,     'String','');
set(handles.deg_polar,    'String','');
set(handles.angle_polar,  'String','');
% Save the handles structure.
guidata(hObject,handles);
imagesc([])


% --- Executes on button press in pushbutton4.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save the handles structure.
set(handles.run_status,'String','Program Is Running');
guidata(hObject,handles);

a        = handles.a;           % between (0,1)
inc_disk = handles.inc_disk;    % in degree
r_obs    = handles.r_obs;       % in unit of M_{BH}
r_disk   = handles.r_disk;      % in unit of M_{BH}
x_size   = handles.x_size;      % half size of the image in x direction
y_size   = handles.y_size;      % half size of the image in y direction
nx       = handles.nx;          % half resolution in x 
ny       = handles.ny;          % half resolution in y    
Gamma    = handles.Gamma;       % photon power law index
nr       = handles.n;           % radial power law index
M        = 1;                   % normalize everything in unit of M_BH
inc_disk = inc_disk*pi/180;     % degree to radian

BRT_input   = {M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny};
num_workers = handles.num_workers;

%--------Create and Run Parallel Jobs on DCSE of Matlab ----------

% strategy 1
% Jm = findResource('scheduler',...
%                  'type',      'jobmanager', ...
%                  'Name',      'JobManager_bchen', ...
%                  'LookupURL', 'boomer.oscer.ou.edu');  %job manager
% remark: replace the host (boomer...) by cluster host at your institution
%      where Matlab Distributed Computing Server is installed.
% J1   = createMatlabPoolJob(Jm);   % create a job
% strategy 2
%J1    = createMatlabPoolJob();
% strategy 3
%J1.MinimumNumberOfWorkers = num_workers; %num_workers;
%cluster = parcluster('Boomer');
%J1      = createJob(cluster);
%task1   = createTask(J1, @BRTP, 5, BRT_input);   % 5 is number of BRT output  
%J1.MaximumNumberOfWorkers = num_workers; %num_workers;              

% strategy 4 (local PCT) uncomment the next line
cluster = parcluster('local');

% strategy for remote parallel computing (uncomment next 6 lines)
%ppn   = 4;
%queue = 'backfill';
%time  = '4:00:00';
%LocalDataLocation  = '/Users/binchen/.matlab/local_cluster_jobs/R2013b'; %Full path to the folder where you store data on YOUR MACHINE
%RemoteDataLocation = '/panfs/storage.local/home-4/bchen3/scratch/matlab/remote'; %Full path to a mat
%cluster = getCluster(ppn, queue, time, LocalDataLocation, RemoteDataLocation);


J1      = createCommunicatingJob(cluster, 'Type', 'pool'); 
J1.NumWorkersRange = [num_workers num_workers];   % fix number of workers
J1.AttachedFiles = {'/Users/binchen/Desktop/Matlab/POLA/pola_matlab'}; %Send all scripts and data files needed for the job
J1.Name = 'KERTAP';
task1 = createTask(J1, @BRTP, 5, BRT_input);    % 5 is number of BRT output 

submit(J1);                                       % submit the job
tic 
wait(J1)            
run_time = toc;                       % ray-tracing finished at this point
pause(20)                            % wait for data transfer
disp('Status of Job: ')
J1.State
J1.SubmitTime
J1.StartTime
J1.FinishTime
out = fetchOutputs(J1);
delete(J1);

t_min    = run_time/60;
t_hour   = run_time/3600;
rate_BRT = (2*nx+1)*(2*ny+1)/run_time;

Image_G = out{1};
Image_Y = out{2};               % not used, reserved for different users
Image_Q = out{3};
Image_P = out{4};
count   = out{5};

numb_disk  = count(1);
numb_hole  = count(2);
numb_high  = count(3);

g_min     = mini_nonzero(Image_G);
g_max     = max(max(Image_G));
err_Q_max = max(max(Image_Q));

%-------------------------------------------------------------------
% compute the strong lensing magnification to specific flux and image area
% I_nu = 1/(r^n*nu^{Gamma-1}) 
dx_grid = x_size/nx;
dy_grid = y_size/ny;
dA_grid = dx_grid*dy_grid;
r_ISCO  = r_MS_plus(M,a);

mu_obs  = cos(inc_disk);
[w_obs,del_obs] = chandra60(mu_obs);    % del_obs is not used.
t0 = 2*pi*mu_obs*w_obs;                 % a temporary variable
if abs(nr-2) < 1E-6                   
    F_flat = t0*log(r_disk/r_ISCO);
else
    F_flat = t0*abs((r_ISCO^(2-nr)-r_disk^(2-nr))/(nr-2));  % unlensed case
end
area_flat = pi*abs(r_disk^2- r_ISCO^2)*mu_obs;      % unlensed area

F_lensed     = 0;
area_lensed  = 0;
for i = 1:2*ny+1
    for j = 1:2*nx+1
        if Image_G(i,j) > 1E-15                 % a good shot
            g_ij = Image_G(i,j);                % read in the redshift
            r_ij = Image_Y(i,j,2);              % read in the r coordinate
            w_ij = Image_P(i,j,1);              % read the angular part
            dF   = w_ij*g_ij^(Gamma+2)/r_ij^nr;       
            F_lensed = F_lensed + dF; 
            area_lensed = area_lensed + 1;
        end 
    end
end
F_lensed = F_lensed*dA_grid;
area_lensed = area_lensed*dA_grid;
mu_flux = F_lensed/F_flat;                   % strong lensing magnification
mu_area = area_lensed/area_flat;

%-------------------------------------------------------------------
% compute specific intensity, and  Stokes parameters

I_total = 0;                           % total intensity initialization
Q_total = 0;    % Stokes parameter U
U_total = 0;    % Stokes parameter V
Image_I = zeros(2*ny+1,2*nx+1);     % image for specific intensity in log10 scale
for i = 1:2*ny+1
    for j = 1:2*nx+1
        if Image_G(i,j) > 1E-15                 % a good shot
            g_ij    = Image_G(i,j);             % read in the redshift
            r_ij    = Image_Y(i,j,2);           % read in the r coordinate
            w_ij    = Image_P(i,j,1);           % limbdarkening, angular weight
            del_ij  = Image_P(i,j,2);           % degree of polarization
            chi_ij  = Image_P(i,j,3);           % degree of polarization
            lens_ij = g_ij^(Gamma+2)/r_ij^nr;   % effect of Kerr lensing
            I_ij    = w_ij*lens_ij;             % intensity in one pixel
            dQ      = del_ij*I_ij*cos(2*chi_ij);
            dU      = del_ij*I_ij*sin(2*chi_ij);
            Q_total = Q_total + dQ;
            U_total = U_total + dU;
            I_total = I_total + I_ij; 
            Image_I(i,j) = log10(abs(I_ij));    % store Intensity in log scale
        end 
    end
end
chi_total = 0.5*atan(U_total/Q_total);                 % radian
del_total = abs(U_total/(I_total*sin(2*chi_total)));   % degree of polarization

%------------------------------------------------------------------
% GUI parameters ouput

set(handles.g_max,      'String', g_max);
set(handles.g_min,      'String', g_min);
set(handles.Q_max,      'String', err_Q_max);
set(handles.BRT_rate,   'String', rate_BRT);
set(handles.wall_clock, 'String', t_hour);
set(handles.run_status, 'String', 'Finished Successfully')
set(handles.flux_mag,   'String', mu_flux)
set(handles.area_mag,   'String', mu_area)
set(handles.deg_polar,  'String', del_total)
set(handles.angle_polar,'String', chi_total*180/pi)
guidata(hObject,handles)

%---------- file output --------------------

dat_str  = datestr(now,'yyyymmmdd@HH:MM');
eps_name = ['./results/inc',num2str(inc_disk*180/pi),'a',num2str(a),...
            '_',dat_str,'.eps'];        %output eps file name
txt_name = ['./results/BRT_',dat_str,'.txt'];
f_id     = fopen(txt_name,'w');         % export the data as ascii

%----- generate the output to  a text file ----------
fprintf(f_id,'nx,ny, total: %6d %6d %12d  \n', nx, ny,(2*nx+1)*(2*ny+1));
fprintf(f_id,'no. of rays hitting the disk,   %10d  \n', numb_disk);
fprintf(f_id,'no. of rays hitting black hole, %10d  \n', numb_hole);
fprintf(f_id,'no. of rays from higher order images, %10d,\n', numb_high);
fprintf(f_id,'g_max = %12.4f, g_min = %12.4f \n',   g_max, g_min);
fprintf(f_id,'mag_flux = %12.4f, mag_area = %12.4f \n',mu_flux, mu_area);
fprintf(f_id,'maximum error in Carter constant,%14.4E \n ', err_Q_max);
fprintf(f_id,'Degree of polarization: %f14.4 \n', del_total);
fprintf(f_id,'Angle of polarization chi (deg): %f14.4\n',chi_total*180/pi);
fprintf(f_id,'number of workers %8d  \n',num_workers);
fprintf(f_id,'RT speed (rays per second) = %12.4f \n', rate_BRT);
fprintf(f_id,'Wall clock time (hours): %f14.4 \n', t_hour);
fclose(f_id);


%----------Generate Redshift Image for GUI, and for indpendent output ----

Image_G2 = Image_G;
for i = 1:2*ny+1
    for j = 1:2*nx+1
        if Image_G2(i,j) < 1E-15    
            Image_G2(i,j) = g_max + 0.; % just for background color
        end 
    end
end

% this generate the plot in the GUI window
imagesc(Image_G2);
colormap('default')
colormap(flipud(colormap))
axis equal
axis off
%colorbar('Location','SouthOutside')
colorbar('off')          % kill the default colorbar
colorbar('FontSize',14)
%colorbar('Location','manual')
colorbar('Position',[0.9,0.15,0.035,0.7])

% redraw the plot for the eps output file
figure(1); 
imagesc(Image_G2)
colormap('default')
colormap(flipud(colormap))
axis equal
axis off
colorbar('FontSize',14)

%---------- Generate Intensity and Polarization plot -----

% generate the sparse vector field data for plot.
ix = floor((2*nx+1)/25);        % say, I want 25 arrows in x-drection
if ix < 3
    ix = 3;                     % minimum stepsize
end
iy = floor((2*ny+1)/25);
if  iy < 3
    iy = 3;
end
[X,Y] = meshgrid(1:ix:2*nx+1,1:iy:2*ny+1);
dim   = size(X);
U = zeros(dim(1),dim(2));
V = zeros(dim(1),dim(2));
i1 = 0;
for i = 1:ix:2*nx+1
    i1 = i1+1;
    j1 = 0;
    for j = 1:iy:2*ny+1
        j1 = j1 + 1;
        if Image_G(j,i) > 1E-15
            delta = Image_P(j,i,2);
            chi   = Image_P(j,i,3);
            U(j1,i1) =  delta*cos(chi);
            V(j1,i1) = -delta*sin(chi); % the overplot with intensity will flip the y-direction
        end
    end
end
%  plot intensity, and polarization data
figure(2); 
imagesc(Image_I)                        % in log scale
colormap('default')
colormap(flipud(colormap))
axis equal
axis off
colorbar('FontSize',14)
hold on
quiver(X,Y,U,V,'r-')
hold off
