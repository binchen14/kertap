This is the matlab version of KERTAP

# Reference: The Astrophysical Journal Supplement Series, 218, 4 (2015)
# Authors:  Bin Chen, Ronald Kantowski, Xinyu Dai, Eddie Baron & Prasad Maddumage
# Affil :   Florida State Univ. & The Univ. of Oklahoma

To run parallel version on a matlab cluster or over a 
multiple core node remotely might be more difficult than 
what you have expected, because the set up depends on the
configuration of the matlab cluster in your institute.

weblinks:

   a. Please refer to official website of MATHWORKS 

	http://www.mathworks.com/help/matlab/

   b. From the Research Computing Center of Florida State Univ, 
      You might found following link useful:

	https://rcc.fsu.edu/software/matlab

To use the GUI of KERTAP

    1. cd to where the code is saved on your computer
    2. create a directory to store results:
	$ mkdir results
    3. start a matlab session
    4. >> KERTAP

If you do not like the GUI, you can modify "main.m"
   slightly, and use it as your matlab version of KERTAP
     
     >> main

"FPP.m" VS "FPP4.m":

    "FPP.m" was an algorithm originally presented in the paper.
    However, we realized later on that the accuracy of the forward
    polarization propagator is not very good if integrated this way.
    Instead, we did a 12D forward runge-kutta solver again.
    The code is a little slower this way, but not much.
    The accuray is much higher, as can be checked by norm of the vector
    |E4|

    "Y_prime_12D.m":
       created for "FPP4.m" and the 12D ode solver.

"KERTAP.fig" and "KERTAP.m":
   
    KERTAP.m is the only long program for this code.
    These two files are generated using MATLAB GUI tool called "guide" 
    Consequently, "KERTAP.m" contains a lot of junks.
    
Matlab version VS Python version:

   If you want to finish a high resolution ray-tracing in a short time,
   consider use the cython version of KERTAP.


