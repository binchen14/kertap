function DY  = Y_prime(xi,Y,para)
%   compute  Y'(xi), where Y is the 8-D phase-space Kerr co-ordinates, 
%   xi the affine parameter
%   para(1) = M (BH mass), 
%   para(2) = a (angular momentum)
%   sovle the ray-tracing problem by using, e.g.,  
% [XI,Yout] = ode45(@Y_prime,[t_i,t_f], Y0,[]     ,para), or
% [XI,Yout] = ode45(@Y_prime,[t_i,t_f], Y0,options,para)
% e.g., options = odeset('RelTol',1e-8,'AbsTol',[1e-4,1e-4,..., 1e-5])

    DY      = zeros(8,1)  ;    % column vector
    
    t       = Y(1)        ;
    r       = Y(2)        ;
    theta   = Y(3)        ;
    phi     = Y(4)        ;
    p_t     = Y(5)        ;
    p_r     = Y(6)        ;
    p_theta = Y(7)        ;
    p_phi   = Y(8)        ;
 
    M = para(1); 
    a = para(2);
    
    sin_theta  = sin(theta);
    cos_theta  = cos(theta);
    sin_cos    = sin_theta*cos_theta;
    sin2       = sin_theta^2;
    
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a); 
    
    Delta2 = Delta^2;
    rho4   = rho2*rho2;
    a2     = a^2;
    r2     = r^2;
    
    s1   =  2*M*a*r/(rho2*Delta);
    s2   =  (Delta -a2*sin2)/(rho2*Delta*sin2);
    
    DY(1,1) = -p_t/alpha2 - s1*p_phi;
    DY(2,1) = Delta*p_r/rho2;
    DY(3,1) = p_theta/rho2;
    DY(4,1) = s2*p_phi - s1*p_t;
     
    s3 = 2*M/(Delta*rho2);
    s4 = (a2+r2)*(a2-r2)/Delta ;            % (a^4-r^4)/Delta
    s5 = 2*r2*a2*sin2/rho2;
    s6 = 4*M*a2*r*(a2+r2)/(Delta*rho4);
    
    reci_alpha2_D_r = s3*(s4 - s5); 
    reci_alpha2_D_th = s6*sin_cos;
    
    s7 = r - M - r*Delta/rho2;
    
    Delta_rho2_D_r  = 2*s7/rho2;
    Delta_rho2_D_th = 2*a2*Delta*sin_cos/rho4;
    
    reci_rho2_D_r   = -2*r/rho4;
    reci_rho2_D_th  =  2*a2*sin_cos/rho4;
    
    s8 = rho2*(a2-r2) - 2*r2*Delta;
    
    minus_g03_D_r =  2*M*a*s8/(rho4*Delta2);    
    minus_g03_D_th = 4*M*r*(a^3)*sin_cos/(rho4*Delta);
    
    s9  = 2*a2*sin2*(r*Delta + (r-M)*rho2)-2*r*Delta2;
    s10 = rho2*(rho2-2*M*r) + 2*M*r*a2*sin2;
    
    g33_D_r  = s9/(rho4*Delta2*sin2);
    g33_D_th = -2*cos_theta*s10/(rho4*Delta*sin_theta^3);
    
% now the momentum space part.       
    
    DY(5,1) = 0;    % p_t, and p_phi are motion constants
    DY(8,1) = 0;
    
% \frac{d p_r}{d\xi},\frac{d p_theta}{d\xi}

    DY(6,1) = 0.5*reci_alpha2_D_r*p_t^2 + minus_g03_D_r*p_t*p_phi  ...
        -0.5*Delta_rho2_D_r*p_r^2 - 0.5*reci_rho2_D_r*p_theta^2 ...
        -0.5*g33_D_r*p_phi^2 ;
    
    DY(7,1) = 0.5*reci_alpha2_D_th*p_t^2 + minus_g03_D_th*p_t*p_phi  ...
        -0.5*Delta_rho2_D_th*p_r^2 - 0.5*reci_rho2_D_th*p_theta^2 ...
        -0.5*g33_D_th*p_phi^2 ;        

end

