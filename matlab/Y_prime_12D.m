function DY  = Y_prime_12D(xi, Y, para)
% DY is 12D column vector. Y can be either row or column.
% Y(i:j) allowed, even if Y is column vector
% size(para) >= 2

    DY        =  zeros(12,1);
    DY(1:8,1) =  Y_prime(xi,Y(1:8),para);
    
    M         = para(1);
    a         = para(2);
    r         =  Y(2);
    theta     =  Y(3);
    p4        =  [Y(5); Y(6); Y(7); Y(8)];
    E4        =  [Y(9),Y(10),Y(11), Y(12)];
    g_up      =  metric_up(r,theta,M,a);
    p_up      =  g_up*p4;
    Gamma     =  connexion(M,a,r,theta);
    for i = 1:4
        for j = 1:4
            for k = 1:4
                DY(i+8,1) = DY(i+8,1) - Gamma(i,j,k)*p_up(j,1)*E4(k);
            end
        end
    end
    
end

