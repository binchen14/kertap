function Gamma = connexion(M,a,r,theta)
% compute the affine connrection Gamma^i_{j,k} for the Kerr metric

    Gamma = zeros(4,4,4);
    a2    = a*a;
    a3    = a2*a;
    r2    = r*r;
    cos1  = cos(theta);
    cos2  = cos1*cos1;
    sin1  = sin(theta);
    sin2  = sin1*sin1;
    sin3  = sin2*sin1;
    sind  = sin(2*theta);
    cot1  = cot(theta); 
    rho2  = r2 + a2*cos2;
    rho4  = rho2*rho2;
    rho6  = rho4*rho2;
    Delta = r2 - 2*M*r +a2;
    w2    = r2 - a2*cos2;
    
    Gamma(1,2,1) = M*(r2+a2)*w2/(Delta*rho4);
    Gamma(1,1,2) = Gamma(1,2,1);
    Gamma(1,3,1) = -a2*M*r*sind/rho4;
    Gamma(1,1,3) = Gamma(1,3,1);
    Gamma(1,4,2) = -a*M*sin2*(rho2*(r2-a2)+2*r2*(r2+a2))/(Delta*rho4);
    Gamma(1,2,4) = Gamma(1,4,2);
    Gamma(1,4,3) = 2*a3*M*r*cos1*sin3/rho4;
    Gamma(1,3,4) = Gamma(1,4,3);
    Gamma(2,1,1) = M*Delta*w2/rho6;
    Gamma(2,2,2) = (r*a2*sin2-M*w2)/(Delta*rho2);
    Gamma(2,3,2) = -a2*sind/(2*rho2);
    Gamma(2,2,3) = Gamma(2,3,2);
    Gamma(2,3,3) = -r*Delta/rho2;
    Gamma(2,4,1) = -a*M*Delta*w2*sin2/rho6;
    Gamma(2,1,4) = Gamma(2,4,1);
    Gamma(2,4,4) = -Delta*sin2*(r*rho4-M*a2*sin2*w2)/rho6;
    Gamma(3,1,1) = -M*a2*r*sind/rho6;
    Gamma(3,2,2) = a2*sind/(2*Delta*rho2);
    Gamma(3,3,2) = r/rho2;
    Gamma(3,2,3) = Gamma(3,3,2);
    Gamma(3,3,3) = -a2*sind/(2*rho2);
    Gamma(3,4,1) = a*M*r*(r2+a2)*sind/rho6;
    Gamma(3,1,4) = Gamma(3,4,1);
    Gamma(3,4,4) = -sind*(rho4*(r2+a2)+2*M*r*a2*sin2*(2*rho2+a2*sin2))/(2*rho6);
    Gamma(4,2,1) = a*M*w2/(Delta*rho4);
    Gamma(4,1,2) = Gamma(4,2,1);
    Gamma(4,3,1) = -2*a*M*r*cot1/rho4;
    Gamma(4,1,3) = Gamma(4,3,1);
    Gamma(4,4,2) = (r*rho4-2*M*r2*rho2-M*a2*sin2*w2)/(Delta*rho4);
    Gamma(4,2,4) = Gamma(4,4,2);
    Gamma(4,4,3) = (rho4+2*M*r*a2*sin2)*cot1/rho4;
    Gamma(4,3,4) = Gamma(4,4,3);
    
end

