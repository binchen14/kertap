function [lookfor,stop,direction] = disk_events(xi,Y,para)
% goal:  Trigger the event when geodesic punches through the equatorial plane
% xi: affine parameter ;    Y:  8D phase-space coordinates
% lookfor: the events theta = pi/2, or cos(theta) = 0.
% stop:  stop the integration or not? 0: no stop, 1: stop
% direction:  0 (no matter),  1 (increasing), -1 (decreasing)
% lookfor, stop, direction are column vectors of same dimension.

    M = para(1);
    a = para(2);
    horizon = M + sqrt(M^2-a^2);            % the outer horizon of Kerr
    del = 0.001;
    horizon_soft = horizon*(1+del);         % soften horizon a little bit.
    flag_BH = (Y(2) <= horizon_soft) - 1 ;  % this is an integer
    
    cos_theta = cos(Y(3));  % (t, r, theta, phi, p_t, p_r, p_theta, p_phi)
    lookfor   = [cos_theta,flag_BH];
    stop      = [1,1];          % stop if disk-hitting, or BHole entering
    direction = [0,0];          % direction does not matter here

    % for each ray with events, there will be only one event, since you
    % stop the ray-tracing at the first event.

end

