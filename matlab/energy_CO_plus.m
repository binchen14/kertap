function  E_disk = energy_CO_plus(Y,M,a)
%  compute energy of a photon w.r.t. comoving observer in the Keplerian
%   flow.

    r     = Y(2);
    theta = Y(3);
    
    g_low = metric_low(r,theta,M,a);
    
    Omega = sqrt(M)/(r^(1.5)+a*sqrt(M));   % Kepelerian angluar velocity  

    s1 = g_low(1,1) + 2*g_low(1,4)*Omega + g_low(4,4)*Omega^2;
    
    gamma = sqrt(abs(-1/s1))   ;           % U^a = gamma*(1,0,0,Omega)
    
    E_disk = gamma*(Y(5) + Omega*Y(8))   ; % U^a*p_a
    
end

