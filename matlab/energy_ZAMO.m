function E_obs = energy_ZAMO(r,theta,M,a,p_low)
% compute energy of an photon as measured by a zero-angular-momentum
% observer
%    u^a = (1/alpha, 0, 0,omega/alpha)
% or u_a = (-\alpha, 0, 0, 0),  u_3 = 0, therefore, zero-angular-momentum

    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a);
    
    s1 = 1/sqrt(alpha2) ;   % s1 = 1/alpha
    
    E_obs = s1*(p_low(1)+ omega*p_low(4));

end

