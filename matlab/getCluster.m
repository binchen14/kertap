function [ cluster ] = getCluster(ppn, queue, rtime, LocalDataLocation, RemoteDataLocation)

%Find the path to id_rsa key file in YOUR system and update the following line
     username = 'bchen3'
     keyfile  = '/Users/binchen/.ssh/id_rsa'; %Your actual path may be DIFFERENT!

%Do not change anything below this line

     if (strcmp(username, 'YOUR HPC USER NAME') == 1)
        disp('You need to put your RCC user name in line 4!')
     	return
     end

     if (exist(keyfile, 'file') == 0)
        disp('Key file path does not exist. Did you configure password-less login to HPC?');
        return
     end

     ClusterMatlabRoot = '/opt/matlab/current';
     clusterHost='submit.hpc.fsu.edu';

     cluster = parcluster('hpc');
     set(cluster,'HasSharedFilesystem',false);
     set(cluster,'JobStorageLocation',LocalDataLocation);
     set(cluster,'OperatingSystem','unix');
     set(cluster,'ClusterMatlabRoot',ClusterMatlabRoot);
     set(cluster,'IndependentSubmitFcn',{@independentSubmitFcn,clusterHost, ...
         RemoteDataLocation,username,keyfile,rtime,queue});
     set(cluster,'CommunicatingSubmitFcn',{@communicatingSubmitFcn,clusterHost, ...
         RemoteDataLocation,username,keyfile,rtime,queue,ppn});
     set(cluster,'GetJobStateFcn',{@getJobStateFcn,username,keyfile});
     set(cluster,'DeleteJobFcn',{@deleteJobFcn,username,keyfile});

