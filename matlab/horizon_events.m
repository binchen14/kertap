function [lookfor,stop,direction] = horizon_events(xi,Y,para)
% goal:  Trigger the event when geodesic enter the horizon
% xi: affine parameter ;    Y:  8D phase-space coordinates
% stop:  stop the integration or not? 0: no stop, 1: stop
% direction:  0 (no matter),  1 (increasing), -1 (decreasing)
% lookfor, stop, direction are column vectors of same dimension.

    M = para(1);
    a = para(2);
    horizon = M + sqrt(M^2-a^2);            % the outer horizon of Kerr
    del = 0.0001;
    horizon_soft = horizon*(1+del);         % soften horizon a little bit.
    flag_BH = (Y(2) <= horizon_soft) - 1 ;  % this is an integer
    
    lookfor   = flag_BH;
    stop      = 1;          % no/yes stop (disk-hitting/Black Hole)
    direction = 0;          % direction does not matter here 

end

