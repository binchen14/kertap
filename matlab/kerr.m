function [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a)
%   compute some important terms in the Kerr metric
%   i.e., \Delta,  \rho^2, \alpha^2, \omega
%   note:  Sig^2 is internal here

    sin_theta = sin(theta);
    cos_theta = cos(theta);
    sin2      = sin_theta^2;
    cos2      = cos_theta^2;
    r2 = r^2;
    a2 = a^2;
    
    rho2      = r2 + a2*cos2;
    Delta     = r2 - 2*M*r + a2;
    Sig2      = rho2*(r2+a2) + 2*M*r*a2*sin2;
    alpha2    = rho2*Delta/Sig2;
    omega     = 2*a*M*r/Sig2;

end

