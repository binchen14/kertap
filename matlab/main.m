
% main.m: a clean version of MATLAB KERTAP, no fancy GUI

% you can change the input parameters in the following block
a        = 0.998;           % between (0,1)
inc_disk = 75.0;            % in degree
r_obs    = 1E6;             % in unit of M_{BH}
r_disk   = 20;              % in unit of M_{BH}
x_size   = 25;              % half size of the image in x direction
y_size   = 25;              % half size of the image in y direction
Gamma    = 2;               % photon power law index
nr       = 3;               % radial power law index
M        = 1;               % normalize everything in unit of M_BH
inc_disk = inc_disk*pi/180; % degree to radian

nx = 50;                    % true size 2*nx+1 
ny = 50;                    %           2*ny+1
num_workers = 4;            % should be <= size of the pool/cluster

%------ end of input parameters block -------------

BRT_input  = {M,a,inc_disk,r_obs,r_disk,x_size,y_size,nx,ny};

%--------Create and Run Parallel Jobs on DCSE of Matlab ----------

% strategy 4 (local PCT) uncomment the next line
cluster = parcluster('local');

%strategy for remote parallel computing (uncomment the next few lines)
%this depends strongly the configuration of your matlab cluster!!!!
% ppn   = 4;             % cores per node
% queue = 'backfill';    % queue to submit the paralle jbo
% time  = '4:00:00';     % runtime 
%following is the Full path to the folder where YOU store data on YOUR MACHINE
% LocalDataLocation  = '/Users/binchen/.matlab/local_cluster_jobs/R2013b'; 
%following is theFull path to remote data location
% RemoteDataLocation = %'/panfs/storage.local/home-4/bchen3/scratch/matlab/remote'; 
% cluster = getCluster(ppn, queue, time, LocalDataLocation, RemoteDataLocation);

%-------------------------------

J1      = createCommunicatingJob(cluster, 'Type', 'pool'); 
J1.NumWorkersRange = [num_workers num_workers];   % fix number of workers

%the following line send all scripts and data files needed for the job
%need for the MCDS case, but not for the local case
%  J1.AttachedFiles = {'/Users/binchen/Desktop/Matlab/POLA/pola_matlab'}; 

J1.Name = 'KERTAP';
task1 = createTask(J1, @BRTP, 5, BRT_input);    % 5 is number of BRT output 

submit(J1);                                       % submit the job
tic 
wait(J1)            
run_time = toc;                       % ray-tracing finished at this point
pause(20)                            % wait for data transfer
disp('Status of Job: ')
J1.State
J1.SubmitTime
J1.StartTime
J1.FinishTime
out = fetchOutputs(J1);
delete(J1);

t_min    = run_time/60;
t_hour   = run_time/3600;
rate_BRT = (2*nx+1)*(2*ny+1)/run_time;

Image_G = out{1};
Image_Y = out{2};               % not used, reserved for different users
Image_Q = out{3};
Image_P = out{4};
count   = out{5};

numb_disk  = count(1);
numb_hole  = count(2);
numb_high  = count(3);

g_min     = mini_nonzero(Image_G);
g_max     = max(max(Image_G));
err_Q_max = max(max(Image_Q));

%-------------------------------------------------------------------
% compute the strong lensing magnification to specific flux and image area
% I_nu = 1/(r^n*nu^{Gamma-1}) 
dx_grid = x_size/nx;
dy_grid = y_size/ny;
dA_grid = dx_grid*dy_grid;
r_ISCO  = r_MS_plus(M,a);

mu_obs  = cos(inc_disk);
[w_obs,del_obs] = chandra60(mu_obs);    % del_obs is not used.
t0 = 2*pi*mu_obs*w_obs;                 % a temporary variable
if abs(nr-2) < 1E-6                   
    F_flat = t0*log(r_disk/r_ISCO);
else
    F_flat = t0*abs((r_ISCO^(2-nr)-r_disk^(2-nr))/(nr-2));  % unlensed case
end
area_flat = pi*abs(r_disk^2- r_ISCO^2)*mu_obs;      % unlensed area

F_lensed     = 0;
area_lensed  = 0;
for i = 1:2*ny+1
    for j = 1:2*nx+1
        if Image_G(i,j) > 1E-15                 % a good shot
            g_ij = Image_G(i,j);                % read in the redshift
            r_ij = Image_Y(i,j,2);              % read in the r coordinate
            w_ij = Image_P(i,j,1);              % read the angular part
            dF   = w_ij*g_ij^(Gamma+2)/r_ij^nr;       
            F_lensed = F_lensed + dF; 
            area_lensed = area_lensed + 1;
        end 
    end
end
F_lensed = F_lensed*dA_grid;
area_lensed = area_lensed*dA_grid;
mu_flux = F_lensed/F_flat;                   % strong lensing magnification
mu_area = area_lensed/area_flat;

%-------------------------------------------------------------------
% compute specific intensity, and  Stokes parameters

I_total = 0;                           % total intensity initialization
Q_total = 0;    % Stokes parameter U
U_total = 0;    % Stokes parameter V
Image_I = zeros(2*ny+1,2*nx+1);     % image for specific intensity in log10 scale
for i = 1:2*ny+1
    for j = 1:2*nx+1
        if Image_G(i,j) > 1E-15                 % a good shot
            g_ij    = Image_G(i,j);             % read in the redshift
            r_ij    = Image_Y(i,j,2);           % read in the r coordinate
            w_ij    = Image_P(i,j,1);           % limbdarkening, angular weight
            del_ij  = Image_P(i,j,2);           % degree of polarization
            chi_ij  = Image_P(i,j,3);           % degree of polarization
            lens_ij = g_ij^(Gamma+2)/r_ij^nr;   % effect of Kerr lensing
            I_ij    = w_ij*lens_ij;             % intensity in one pixel
            dQ      = del_ij*I_ij*cos(2*chi_ij);
            dU      = del_ij*I_ij*sin(2*chi_ij);
            Q_total = Q_total + dQ;
            U_total = U_total + dU;
            I_total = I_total + I_ij; 
            Image_I(i,j) = log10(abs(I_ij));    % store Intensity in log scale
        end 
    end
end
chi_total = 0.5*atan(U_total/Q_total);                 % radian
del_total = abs(U_total/(I_total*sin(2*chi_total)));   % degree of polarization


%---------- file output --------------------

dat_str  = datestr(now,'yyyymmmdd@HH:MM');
eps_name = ['./results/inc',num2str(inc_disk*180/pi),'a',num2str(a),...
            '_',dat_str,'.eps'];        %output eps file name
txt_name = ['./results/BRT_',dat_str,'.txt'];
f_id     = fopen(txt_name,'w');         % export the data as ascii

%----- generate the output to  a text file ----------
fprintf(f_id,'nx,ny, total: %6d %6d %12d  \n', nx, ny,(2*nx+1)*(2*ny+1));
fprintf(f_id,'no. of rays hitting the disk,   %10d  \n', numb_disk);
fprintf(f_id,'no. of rays hitting black hole, %10d  \n', numb_hole);
fprintf(f_id,'no. of rays from higher order images, %10d,\n', numb_high);
fprintf(f_id,'g_max = %12.4f, g_min = %12.4f \n',   g_max, g_min);
fprintf(f_id,'mag_flux = %12.4f, mag_area = %12.4f \n',mu_flux, mu_area);
fprintf(f_id,'maximum error in Carter constant,%14.4E \n ', err_Q_max);
fprintf(f_id,'Degree of polarization: %f14.4 \n', del_total);
fprintf(f_id,'Angle of polarization chi (deg): %f14.4\n',chi_total*180/pi);
fprintf(f_id,'number of workers %8d  \n',num_workers);
fprintf(f_id,'RT speed (rays per second) = %12.4f \n', rate_BRT);
fprintf(f_id,'Wall clock time (hours): %f14.4 \n', t_hour);
fclose(f_id);


%----------Generate Redshift Image for GUI, and for indpendent output ----

Image_G2 = Image_G;
for i = 1:2*ny+1
    for j = 1:2*nx+1
        if Image_G2(i,j) < 1E-15    
            Image_G2(i,j) = g_max + 0.; % just for background color
        end 
    end
end

% this generate the plot in the GUI window
imagesc(Image_G2);
colormap('default')
colormap(flipud(colormap))
axis equal
axis off
%colorbar('Location','SouthOutside')
colorbar('off')          % kill the default colorbar
colorbar('FontSize',14)
%colorbar('Location','manual')
colorbar('Position',[0.9,0.15,0.035,0.7])

% redraw the plot for the eps output file
figure(1); 
imagesc(Image_G2)
colormap('default')
colormap(flipud(colormap))
axis equal
axis off
colorbar('FontSize',14)

%---------- Generate Intensity and Polarization plot -----

% generate the sparse vector field data for plot.
ix = floor((2*nx+1)/25);        % say, I want 25 arrows in x-drection
if ix < 3
    ix = 3;                     % minimum stepsize
end
iy = floor((2*ny+1)/25);
if  iy < 3
    iy = 3;
end
[X,Y] = meshgrid(1:ix:2*nx+1,1:iy:2*ny+1);
dim   = size(X);
U = zeros(dim(1),dim(2));
V = zeros(dim(1),dim(2));
i1 = 0;
for i = 1:ix:2*nx+1
    i1 = i1+1;
    j1 = 0;
    for j = 1:iy:2*ny+1
        j1 = j1 + 1;
        if Image_G(j,i) > 1E-15
            delta = Image_P(j,i,2);
            chi   = Image_P(j,i,3);
            U(j1,i1) =  delta*cos(chi);
            V(j1,i1) = -delta*sin(chi); % the overplot with intensity will flip the y-direction
        end
    end
end
%  plot intensity, and polarization data
figure(2); 
imagesc(Image_I)                        % in log scale
colormap('default')
colormap(flipud(colormap))
axis equal
axis off
colorbar('FontSize',14)
hold on
quiver(X,Y,U,V,'r-')
hold off
