function g_low = metric_low(r,theta,M,a)
% compute the covariant metric tensor for Kerr metric in Boyer-Lindquist
% with (-,+,+,+) signature
% where g_low is a 4by4 matrix, with index  1-4 (t,r,theta,phi)

    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a);
        
    sin2 = sin(theta)^2;
    a2   = a*a;
    r2   = r*r;
    
    Sig2 = rho2*(r2+a2) + 2*M*r*a2*sin2;
    
    g_low = zeros(4,4);
    
    s1 = 2*M*r/rho2;
    
    % covariant metric tensor;
    
    g_low(1,1) = -(1-s1);
    g_low(2,2) = rho2/Delta;
    g_low(3,3) = rho2;
    g_low(4,4) = Sig2*sin2/rho2;
    
    g_low(1,4) = -s1*a*sin2;
    g_low(4,1) = g_low(1,4);

end

