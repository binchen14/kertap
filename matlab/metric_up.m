function g_up = metric_up(r,theta,M,a)
% compute the contravariant metric tensor for Kerr metric
% where g_up is a 4by4 matrix, with index  1-4 (t,r,theta,phi)

    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a) ;
        
    sin2 = sin(theta)^2;
    a2   = a*a;
    r2   = r*r;
    
    g_up = zeros(4,4);
    
    s1 = 2*M*r/rho2;
    
    % contravarant metric tensor;
    
    g_up(1,1) = -1/alpha2;
    g_up(2,2) =  Delta/rho2;
    g_up(3,3) =  1/rho2;
    g_up(4,4) =  (Delta-a2*sin2)/(rho2*Delta*sin2);
    
    g_up(1,4) = -s1*a/Delta;
    g_up(4,1) =  g_up(1,4);

end

