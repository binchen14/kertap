function mini = mini_nonzero(data)
% compute the minimum nonzero number of data
% assume data >= 0 for all entries.

    dim = size(data);
    n_row = dim(1);
    n_col = dim(2);
    
    data_fix = zeros(n_row,n_col);
    for i = 1:n_row
        for j = 1:n_col
            if data(i,j)  < 1E-15      % consider as zero
                data_fix(i,j) = 1E50;  % replace by a large number
            else
                data_fix(i,j) =  data(i,j); 
            end
            
        end
    end

    mini = min(min(data_fix));

end

