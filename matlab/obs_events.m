function [lookfor,stop,direction] = obs_events(xi,Y,para)
% goal:  Trigger the event when geodesic approaches the r_obs sphere
% xi: affine parameter ;    Y:  12D phase-space coordinates
% lookfor: the events r = r_obs.
% stop:  stop the integration or not? 0: no stop, 1: stop
% direction:  0 (no matter),  1 (increasing), -1 (decreasing)
% lookfor, stop, direction are column vectors of same dimension.

    r_obs     = para(3);
    dr_obs    = Y(2)-r_obs;
    lookfor   = [dr_obs];
    stop      = [1];          % stop if disk-hitting, or BHole entering
    direction = [0];          % direction does not matter here

end

