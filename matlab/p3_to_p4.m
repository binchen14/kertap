function p4_low  = p3_to_p4(p3,r,theta,M,a)
%   convert 3D vector  (w.r.t. orthonormal triad, e_r, e_theat, e_phi)
%   to 4-D covariant momentum vector, p4_low, w.r.t. Kerr coordinate basis.
%   used for initializing the ray-tracing.
        
    [Delta,rho2,alpha2,omega] = kerr(r,theta,M,a);
    
    g_low = metric_low(r,theta,M,a);
    
    p_r     = p3(1);
    p_theta = p3(2);
    p_phi   = p3(3);   
    
    norm = sqrt(p_r^2 + p_theta^2 + p_phi^2);
    
    p_r     =     p_r/norm;
    p_theta = p_theta/norm;
    p_phi   =   p_phi/norm;
    
    alpha = sqrt(abs(alpha2));
        
    p_up    = zeros(4,1);
    
    p_up(1) = 1/alpha;
    p_up(2) =                 p_r/sqrt(g_low(2,2));
    p_up(3) =             p_theta/sqrt(g_low(3,3));
    p_up(4) = omega/alpha + p_phi/sqrt(g_low(4,4));
    
    p4_low = g_low*p_up;   % contravariant to covariant 4-vector. 
    
end

