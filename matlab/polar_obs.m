function chi = polar_obs(X0,E_obs,M,a)
% compute the polarization angle at the observer
%   chi is in radian, measured w.r.t. the e_phi direction at the observer
%   assume E_obs a 4D COLUMN vector;
%   assume X0    a 4D ROW    vector;
%   assume chi  is  

    s1        = 1E-60;          % soften the denominator avoid zeros
    r         = X0(2);
    theta     = X0(3);
    sin_theta = sin(theta);
    cos_theta = cos(theta);
    sin2      = sin_theta^2;
    cos2      = cos_theta^2;
    r2        = r^2;
    a2        = a^2;
    
    rho2      = r2 + a2*cos2;
    Delta     = r2 - 2*M*r + a2;
    Sig2      = rho2*(r2+a2) + 2*M*r*a2*sin2;
    alpha2    = rho2*Delta/Sig2;
    omega     = 2*a*M*r/Sig2;

    g44       = Sig2*sin2/rho2;
    g22       = rho2/Delta;
    g33       = rho2;
    
    alpha     = sqrt(alpha2);
    
    M_zamo_coord_to_hat = ...
        [           alpha,         0,          0,        0;    ...
                        0, sqrt(g22),          0,        0;    ...
                        0,         0,  sqrt(g33),        0;    ...
         -sqrt(g44)*omega,         0,          0, sqrt(g44)];

    E_obs_hat    = M_zamo_coord_to_hat*E_obs;  
    
    E_hat_theta  = E_obs_hat(3,1);    % point downward
    E_hat_phi    = E_obs_hat(4,1);    % point to the right
    chi          = 0.5*atan(-E_hat_theta/(E_hat_phi+s1));
    
end

