function [weight, delta, E]= polarization(Y,M,a)
%  compute the initial polarization vector E on the accretion disk
%     given photon's 8D phase-space coordinates Y(1:8)
%        E is a 4 row column vector. 
%        delta is the degree of polarization
%        weight is the limbdarkening factor.
%        the table used of interpolation comes from Chandra1960 Tab XXIV

    r     = Y(2);
    theta = Y(3);
    p_low = Y(5:8);
    g_low = metric_low(r,theta,M,a);
    g_up  =  metric_up(r,theta,M,a);    
    p_up  = g_up*p_low.';                    % p_low is row vector       
    
    Omega_K = sqrt(M)/(r^(1.5)+a*sqrt(M));   % Kepelerian angluar velocity
    
    g11    = g_low(1,1);
    g14    = g_low(1,4);
    g44    = g_low(4,4);
    g22    = g_low(2,2);
    g33    = g_low(3,3);
    
    s1     = g11 + 2*g14*Omega_K + g44*Omega_K^2;
    gamma  = sqrt(abs(-1/s1));           % U^a = gamma*(1,0,0,Omega)
    lambda = -(g14 + g44*Omega_K)/(g11 + g14*Omega_K);
    tau    = 1/sqrt(g11*lambda^2 + 2*g14*lambda + g44);
    
    s2     = 1 - lambda*Omega_K; 
    
    M_coord_to_hat = ...
        [1/(gamma*s2),         0,          0, -lambda/(gamma*s2); ...
                    0, sqrt(g22),          0, 0; ... 
                    0,         0, -sqrt(g33), 0; ...
        -Omega_K/(tau*s2),     0,          0, 1/(tau*s2)];   
    
    p_hat  = M_coord_to_hat*p_up;      % with respect to the tetrad
    
    norm3  = sqrt(p_hat(2)^2 + p_hat(3)^2 + p_hat(4)^2);
    mu     = abs(p_hat(3))/norm3;      % the upward normal to equatororial
    
    s3     = 1/sqrt(p_hat(2)^2 + p_hat(4)^2);
    E_hat  = s3*[0; -p_hat(4); 0; p_hat(2)];
    
    M_hat_to_coord = ...
        [        gamma,           0,            0,   tau*lambda; ...
                     0, 1/sqrt(g22),            0,            0; ... 
                     0,           0, -1/sqrt(g33),            0; ...
         gamma*Omega_K,           0,            0,          tau ];  
     
    E       = M_hat_to_coord*E_hat;
    
    [weight,delta] = chandra60(mu);
    
    
end

