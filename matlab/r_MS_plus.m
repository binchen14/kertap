function  r_ISCO = r_MS_plus(M,a)
%   compute the innermost stable circular orbit for prograde orbit
%   Detailed explanation goes here

    s  = a/M;
    s2 = s*s;
    Z1 = 1 + (1-s2)^(1/3)*((1+s)^(1/3) + (1-s)^(1/3));
    Z2 = sqrt(3*s2 + Z1*Z1);
    
    r_ISCO = M*(3 + Z2 - sqrt((3-Z1)*(3 + Z1 + 2*Z2)) );
   

end

